function currentSlide(n) {
    showGallery(slideIndex = n);
}

function nextSlide(n) {
    showGallery(slideIndex += n);
}

function showGallery(n) {
    var i;
    var slides = document.getElementsByClassName("slide");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" activeDot", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " activeDot";
}

function initMap() {
    var address = {lat: 59.944156, lng: 30.328298};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: address
    });
    var marker = new google.maps.Marker({
        position: address,
        map: map
    });
}